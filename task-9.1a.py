'''
Задание 9.1a
Сделать копию функции из задания 9.1.

Дополнить скрипт: ввести дополнительный параметр, который контролирует будет ли настроен port-security:

имя параметра „psecurity“
по умолчанию значение None
для настройки port-security, как значение надо передать список команд port-security (находятся в списке port_security_template)
Функция должна возвращать список всех портов в режиме access с конфигурацией на основе шаблона access_mode_template и шаблона port_security_template, если он был передан. В конце строк в списке не должно быть символа перевода строки.

Проверить работу функции на примере словаря access_config, с генерацией конфигурации port-security и без.

Ограничение: Все задания надо выполнять используя только пройденные темы.

access_mode_template = [
    'switchport mode access', 'switchport access vlan',
    'switchport nonegotiate', 'spanning-tree portfast',
    'spanning-tree bpduguard enable'
]

port_security_template = [
    'switchport port-security maximum 2',
    'switchport port-security violation restrict',
    'switchport port-security'
]

access_config = {
    'FastEthernet0/12': 10,
    'FastEthernet0/14': 11,
    'FastEthernet0/16': 17
}
'''

def generate_access_config(intf_vlan_mapping, access_config, psecurity = 'None'):
    for word in intf_vlan_mapping:
        if psecurity == 'None':
            print('{},'.format(word), '\n','{},'.format(access_config[0]),
                  '{} {},'.format(access_config[1], intf_vlan_mapping[word]),
                  '{}, {}, {}'.format(access_config[2], access_config[3], access_config[4]))
        else:
            print('{},'.format(word), '\n', '{},'.format(access_config[0]),
                  '{} {},'.format(access_config[1], intf_vlan_mapping[word]),
                  '{}, {}, {}, '.format(access_config[2], access_config[3], access_config[4]),
                  '{}, {}, {} '.format(*port_security_config))


access_config = [
    'switchport mode access', 'switchport access vlan',
    'switchport nonegotiate', 'spanning-tree portfast',
    'spanning-tree bpduguard enable'
]

port_security_config = [
    'switchport port-security maximum 2',
    'switchport port-security violation restrict',
    'switchport port-security'
]

intf_vlan_mapping = {
    'FastEthernet0/12':10,
    'FastEthernet0/14':11,
    'FastEthernet0/16':17
}

print(generate_access_config(intf_vlan_mapping, access_config, psecurity = 'Yes'))

print(generate_access_config(intf_vlan_mapping, access_config))

print(generate_access_config(intf_vlan_mapping, access_config, psecurity='None'))