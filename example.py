import operator


a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]



'''
Самый простой вариант, который первым приходит на ум — использовать цикл for:

for elem in a:
    if elem < 5:
        print(elem)
Также можно воспользоваться функцией filter, которая фильтрует элементы согласно заданному условию:

print(list(filter(lambda elem: elem < 5, a)))
И, вероятно, наиболее предпочтительный вариант решения этой задачи — списковое включение:

print([elem for elem in a if elem < 5])
'''




a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
c = list()
for i in range(0,len(a)):
    c = c + [elem for elem in b if elem == a[i]]
print(list(set(c)))

'''vНужно вернуть список, который состоит из элементов, общих для этих двух списков.

Вариант решения
И снова мы можем воспользоваться циклом for:

result = []
for elem in a:
    if elem in b:
        result.append(elem)
Или функцией filter:

result = list(filter(lambda elem: elem in b, a))
Или списковым включением:

result = [elem for elem in a if elem in b]

А можно привести оба списка к множествам и найти их пересечение:

result = list(set(a) & set(b))

Однако в таком случае каждый элемент встретится в результирующем списке лишь один раз, т.к. множество поддерживает уникальность входящих в него элементов. Первые два решения (с фильтрацией) оставят все дубли на своих местах.
'''




d = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}

'''
Сортируем в порядке возрастания:

result = dict(sorted(d.items(), key=operator.itemgetter(1)))
И в порядке убывания:

result = dict(sorted(d.items(), key=operator.itemgetter(1), reverse=True))
'''
result = dict(sorted(d.items(), key=operator.itemgetter(1), reverse=True))

print(result)



dict_a = {1:10, 2:20}
dict_b = {3:30, 4:40}
dict_c = {5:50, 6:60}

dict_d = {**dict_a, **dict_b, **dict_c}
print({**dict_a, **dict_b, **dict_c})

my_dict = {'a':500, 'b':5874, 'c': 560,'d':400, 'e':5874, 'f': 20}
result = sorted(my_dict, key=my_dict.get, reverse=True)[:3]
#result = dict(sorted(my_dict.items(), key=operator.itemgetter(1))[:2:-1])
#print(result.keys())
print(result)


print(int('ABC', 16))




#abc = 'cat'

def polindrom(abc):

    return (print(''.join([elem for elem in abc if abc == abc[::-1]])))
polindrom('aaa')

import datetime

print(str(datetime.timedelta(seconds=766)))