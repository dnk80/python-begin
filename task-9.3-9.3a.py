'''
Задание 9.3
Создать функцию get_int_vlan_map, которая обрабатывает конфигурационный файл коммутатора и возвращает кортеж из двух словарей:

словарь портов в режиме access, где ключи номера портов, а значения access VLAN:
{'FastEthernet0/12': 10,
 'FastEthernet0/14': 11,
 'FastEthernet0/16': 17}
словарь портов в режиме trunk, где ключи номера портов, а значения список разрешенных VLAN:
У функции должен быть один параметр config_filename, который ожидает как аргумент имя конфигурационного файла.

Проверить работу функции на примере файла config_sw1.txt

Ограничение: Все задания надо выполнять используя только пройденные темы.

Задание 9.3a
Сделать копию функции get_int_vlan_map из задания 9.3.

Дополнить функцию: добавить поддержку конфигурации, когда настройка access-порта выглядит так:

interface FastEthernet0/20
    switchport mode access
    duplex auto
То есть, порт находится в VLAN 1

В таком случае, в словарь портов должна добавляться информация, что порт в VLAN 1

У функции должен быть один параметр config_filename, который ожидает как аргумент имя конфигурационного файла.

Проверить работу функции на примере файла config_sw2.txt

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''



def get_int_vlan_map(config_filename = 'config_sw1.txt'):
    trunk_config = {}
    access_config = {}
    with open(config_filename, 'r') as flow:
        i = str()
        # file in one line
        for record in flow.read():
            i = (i + record).replace('\n', ',')
        for result in i.split('!'):

            if result.__contains__('thernet') and result.count('access')==0 and result.count('trunk')==0:
                for line in result.split(','):
                    if line.__contains__('duplex auto') and result.count('switchport access vlan')==0:
                        access_config[result.split(',')[1][10::]] = '1'

            elif result.__contains__('thernet') and result.count('switchport access vlan') > 0:
                for line in result.split(','):
                    if line.__contains__('switchport access vlan'):
                        access_config[result.split(',')[1][10::]] = str(line.split(' ')[4::]).strip('[]').replace('\'', '')

            elif result.__contains__('thernet') and result.count('access')==0 and result.count('trunk')>1:
                for line in result.split(', '):
                    #print(line)
                    if line.__contains__('switchport trunk allowed vlan'):
                        trunk_config[result.split(',')[1][10::]] = line.replace('switchport trunk allowed vlan ', '')

    print('trunk_config = ', trunk_config, '\n', 'access_config = ', access_config)
    return ('')


print(get_int_vlan_map(config_filename = 'config_sw2.txt'))