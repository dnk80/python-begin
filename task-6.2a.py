#!/bin/env python3

'''
Задание 6.2a
Сделать копию скрипта задания 6.2.

Добавить проверку введенного IP-адреса. Адрес считается корректно заданным, если он:

состоит из 4 чисел разделенных точкой,
каждое число в диапазоне от 0 до 255.
Если адрес задан неправильно, выводить сообщение: „Неправильный IP-адрес“

Задание 6.2
Запросить у пользователя ввод IP-адреса в формате 10.0.1.1
Определить тип IP-адреса.
В зависимости от типа адреса, вывести на стандартный поток вывода:
„unicast“ - если первый байт в диапазоне 1-223
„multicast“ - если первый байт в диапазоне 224-239
„local broadcast“ - если IP-адрес равен 255.255.255.255
„unassigned“ - если IP-адрес равен 0.0.0.0
„unused“ - во всех остальных случаях

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

try:
    ip = input('Enter IP address:')
    if int(ip.split('.')[0]) < 256 and int(ip.split('.')[0]) > -1 and int(ip.split('.')[1]) < 256 and int(ip.split('.')[1]) > -1 and int(ip.split('.')[2]) < 256 and int(ip.split('.')[2]) > -1 and int(ip.split('.')[3]) < 256 and int(ip.split('.')[3]) > -1 and ip.count('.') == 3:
        print ('Ip correct')
    else:
        print('Incorrect input')
except (ValueError):
    print ("You must input only digit with demark dot")
else:
        if int(ip.split('.')[0]) < 224 and int(ip.split('.')[0]) > 0 and ip.count('.') == 3:
            print('unicast')
        elif int(ip.split('.')[0]) > 223 and int(ip.split('.')[0]) < 240 and ip.count('.') == 3:
            print('multicast')
        elif int(ip.split('.')[0]) == 255 and int(ip.split('.')[1]) == 255 and int(ip.split('.')[2]) == 255 and int(ip.split('.')[3]) == 255 and ip.count('.') == 3:
            print('local broadcast')
        elif int(ip.split('.')[0]) == 0 and int(ip.split('.')[1]) == 0 and int(ip.split('.')[2]) == 0 and int(ip.split('.')[3]) == 0 and ip.count('.') == 3:
            print('unassigned')
        else:
            print('Unused')

