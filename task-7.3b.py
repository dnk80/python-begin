#!/bin/env python3
'''
Задание 7.3
Скрипт должен обрабатывать записи в файле CAM_table.txt. Каждая строка, где есть MAC-адрес, должна быть обработана таким образом, чтобы на стандартный поток вывода была выведена таблица вида (показаны не все строки из файла):

100    01bb.c580.7000   Gi0/1
200    0a4b.c380.7000   Gi0/2
300    a2ab.c5a0.7000   Gi0/3
100    0a1b.1c80.7000   Gi0/4
500    02b1.3c80.7000   Gi0/5
200    1a4b.c580.7000   Gi0/6
300    0a1b.5c80.7000   Gi0/7
Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

'''
Задание 7.3a
Сделать копию скрипта задания 7.3.

Дополнить скрипт: Отсортировать вывод по номеру VLAN.

В результате должен получиться такой вывод:

10       01ab.c5d0.70d0      Gi0/8
10       0a1b.1c80.7000      Gi0/4
100      01bb.c580.7000      Gi0/1
200      0a4b.c380.7c00      Gi0/2
200      1a4b.c580.7000      Gi0/6
300      0a1b.5c80.70f0      Gi0/7
300      a2ab.c5a0.700e      Gi0/3
500      02b1.3c80.7b00      Gi0/5
1000     0a4b.c380.7d00      Gi0/9
Ограничение: Все задания надо выполнять используя только пройденные темы.
'''
'''
Задание 7.3b
Сделать копию скрипта задания 7.3a.

Дополнить скрипт:

Запросить у пользователя ввод номера VLAN.
Выводить информацию только по указанному VLAN.
Ограничение: Все задания надо выполнять используя только пройденные темы.
'''


CAM_table = '''
sw1#sh mac address-table
          Mac Address Table
-------------------------------------------

Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
 100    01bb.c580.7000    DYNAMIC     Gi0/1
 200    0a4b.c380.7c00    DYNAMIC     Gi0/2
 300    a2ab.c5a0.700e    DYNAMIC     Gi0/3
 10     0a1b.1c80.7000    DYNAMIC     Gi0/4
 500    02b1.3c80.7b00    DYNAMIC     Gi0/5
 200    1a4b.c580.7000    DYNAMIC     Gi0/6
 300    0a1b.5c80.70f0    DYNAMIC     Gi0/7
 10     01ab.c5d0.70d0    DYNAMIC     Gi0/8
 1000   0a4b.c380.7d00    DYNAMIC     Gi0/9
'''

#Write to file in non popular method - academic )) with open / close function file
f = open('CAM_table.txt', 'w')

for line in CAM_table:
    f.write(line)
f.close()


from sys import argv
## in case when script start as "python3 <script_name config_sw1.txt>
file ='CAM_table.txt'
## in case interactive question please input connfig_sw1.txt>
#file = input('Введите аргумент config_sw1.txt \n')
vl = input('Введите номер влана \n')

with open(file, 'r') as flow:
    for record in flow:
        if record.strip().startswith('----') or record.strip().startswith('Vlan') or record.strip().startswith('Mac') or record.strip().__contains__('address-table') or record.strip().__len__()==0:
            i='i' #Заглушка
        elif str(list(str(record.strip().split(' '))[2:8].replace('\'', ' ').split(' ')[0:1:])).strip('[]').strip('\'') == vl:
            print(record)
'''
        else :

            a = str(record.strip().split(' '))[2:8]
            #print(a)
            j = list(a.replace('\'', ' ').split(' ')[0:1:])
            fr = str(list(a.replace('\'', ' ').split(' ')[0:1:])).strip('[]').strip('\'')
            #print(str(list(a.replace('\'', ' ').split(' '))[0:1:]).replace('\'','').strip('[]'))
            #vlan =  str(record.strip().split(' '))[2:8].replace('\'', ' ').split(' ')
            print(fr)
vlans = [1, 50, 10, 15]
vlans.sort()
'''