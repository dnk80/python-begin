#! /usr/bin/env python3
# -*- coding: utf-8 -*-

txt = """
Задание 4.6
Обработать строку ospf_route и вывести информацию на стандартный поток вывода в виде:

Protocol:               OSPF
Prefix:                 10.0.24.0/24
AD/Metric:              110/41
Next-Hop:               10.0.13.3
Last update:            3d18h
Outbound Interface:     FastEthernet0/0
Ограничение: Все задания надо выполнять используя только пройденные темы.

ospf_route = 'O        10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0'
"""

string = """
Protocol:               OSPF
Prefix:                 10.0.24.0/24
AD/Metric:              110/41
Next-Hop:               10.0.13.3
Last update:            3d18h
Outbound Interface:     FastEthernet0/0
"""

print('ospf_route =', string.split()[1].strip('SPF'))

print('ospf_route =', string.split()[1].strip('SPF'), string.split('\n')[2].replace('Prefix:',''),string.split('\n')[3].replace('AD/Metric:','').replace('[','').replace(']',''), ' via ',string.split()[7].replace('.3','.3,'), string.split()[10].replace('8h','8h,'), string.split()[13])