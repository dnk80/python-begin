#!/bin/env python3

'''
Задание 7.1
Аналогично заданию 4.6 обработать строки из файла ospf.txt и вывести информацию по каждой в таком виде:

Protocol:              OSPF
Prefix:                10.0.24.0/24
AD/Metric:             110/41
Next-Hop:              10.0.13.3
Last update:           3d18h
Outbound Interface:    FastEthernet0/0
Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

ospf_txt = '''
O        10.0.24.0/24 [110/41] via 10.0.13.3, 3d18h, FastEthernet0/0
O        10.0.28.0/24 [110/31] via 10.0.13.3, 3d20h, FastEthernet0/0
O        10.0.37.0/24 [110/11] via 10.0.13.3, 3d20h, FastEthernet0/0
O        10.0.41.0/24 [110/51] via 10.0.13.3, 3d20h, FastEthernet0/0
O        10.0.78.0/24 [110/21] via 10.0.13.3, 3d20h, FastEthernet0/0
O        10.0.79.0/24 [110/20] via 10.0.19.9, 4d02h, FastEthernet0/2
O        10.0.81.0/24 [110/41] via 10.0.13.3, 3d20h, FastEthernet0/0
O        10.0.91.0/24 [110/60] via 10.0.19.9, 3d19h, FastEthernet0/2
'''

#Write to file in non popular method - academic )) with open / close function file
f = open('ospf.txt', 'w')

for line in ospf_txt:
    f.write(line)
f.close()

out = '''
Protocol:               {}
Prefix:                 {}
AD/Metric:              {}
Next-Hop:               {}
Last update:            {}
Outbound Interface:     {}
'''

with open('ospf.txt', 'r') as flow:
    for record in flow:
        for dsc in out.splitlines():
            if dsc.startswith('Protocol:'):
                for part in record.split():
                    if part == 'O':
                        part = 'OSPF' #replace O on OSPF
                        print(dsc.format(part))
            elif dsc.startswith('Prefix:'):
                for part in record.split():
                     if part.__contains__('/') and part.count('.') == 3: #search string where are contain symbol / and three dots .
                        print(dsc.format(part))
            elif dsc.startswith('AD/Metric:'):
                for part in record.split():
                     if part.__contains__('[') and part.__contains__(']') and part.count('/') == 1: #search string where are contain symbols [ ] and contain /
                        part = part.strip('[]') # cut char [ ]
                        print(dsc.format(part))
            elif dsc.startswith('Next-Hop:'):
                for part in record.split():
                     if part.__contains__('.') and part.count('/') == 0: #search string where are contain dots . and not contain symbol /
                        part = part.strip(',')  # cut char ,
                        print(dsc.format(part))
            elif dsc.startswith('Last update:'):
                for part in record.split():
                     if part.__contains__('d') or part.__contains__('h') and len(part) < 10:
                        part = part.strip(',') # cut char ,
                        print(dsc.format(part))
            elif dsc.startswith('Outbound Interface:'):
                for part in record.split():
                     if part.__contains__('net') or part.__contains__('back'):
                        print(dsc.format(part))
        print('\n')