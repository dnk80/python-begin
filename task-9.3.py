'''
Задание 9.3
Создать функцию get_int_vlan_map, которая обрабатывает конфигурационный файл коммутатора и возвращает кортеж из двух словарей:

словарь портов в режиме access, где ключи номера портов, а значения access VLAN:
{'FastEthernet0/12': 10,
 'FastEthernet0/14': 11,
 'FastEthernet0/16': 17}
словарь портов в режиме trunk, где ключи номера портов, а значения список разрешенных VLAN:
У функции должен быть один параметр config_filename, который ожидает как аргумент имя конфигурационного файла.

Проверить работу функции на примере файла config_sw1.txt

Ограничение: Все задания надо выполнять используя только пройденные темы.

Задание 9.3a
Сделать копию функции get_int_vlan_map из задания 9.3.

Дополнить функцию: добавить поддержку конфигурации, когда настройка access-порта выглядит так:

interface FastEthernet0/20
    switchport mode access
    duplex auto
То есть, порт находится в VLAN 1

В таком случае, в словарь портов должна добавляться информация, что порт в VLAN 1

У функции должен быть один параметр config_filename, который ожидает как аргумент имя конфигурационного файла.

Проверить работу функции на примере файла config_sw2.txt

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

ignore = ['duplex', 'alias', 'Current']
command='!'
def convert_config_to_dict(config_filename = 'config_sw1.txt'):
    config={}
    tmp={}
    with open(config_filename, 'r') as flow:
        i = str()
        # file in one line
        for record in flow.read():
            i = (i + record).replace('\n', '##')
        for result in i.split('!'):
            #print(result)
            if result.count('##')==1:
                #print(result)
                result
            else:
                #print(result.split('##')[1:-1])
                for line in result.split('##')[1:-1]:
                    p = '0'
                    #print(line)
                    if line[0:1:].count(' ')==0 and not line.__contains__(ignore[1]) and not line.__contains__(ignore[2]):
                        tmp[result.split('##')[1]] = ''
                        config = tmp
                    elif line[0:1:].count(' ')==1 and not line.__contains__(ignore[0]):
                        #print(type(line[1::]+','))
                        tmp[result.split('##')[1]] = tmp[result.split('##')[1]] + (line[1::] + ',')
                        config = tmp


    print(config)
    return ''

print(convert_config_to_dict(config_filename='config_sw1.txt'))

# доделать с помощью форматированного выводв словарь где вместо стингового значения list, сейчас уже облом, заебало