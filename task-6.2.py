'''
Задание 6.2
Запросить у пользователя ввод IP-адреса в формате 10.0.1.1
Определить тип IP-адреса.
В зависимости от типа адреса, вывести на стандартный поток вывода:
„unicast“ - если первый байт в диапазоне 1-223
„multicast“ - если первый байт в диапазоне 224-239
„local broadcast“ - если IP-адрес равен 255.255.255.255
„unassigned“ - если IP-адрес равен 0.0.0.0
„unused“ - во всех остальных случаях
Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

ip=input('Enter IP address:')

if int(ip.split('.')[0]) < 223 and int(ip.split('.')[0]) > 1:
    print('unicast')
elif int(ip.split('.')[0]) > 224 and int(ip.split('.')[0]) < 239:
    print('multicast')
elif int(ip.split('.')[0]) == 255 and int(ip.split('.')[1]) == 255 and int(ip.split('.')[2]) == 255 and int(ip.split('.')[3]) == 255:
    print('local broadcast')
elif int(ip.split('.')[0]) == 0 and int(ip.split('.')[1]) == 0 and int(ip.split('.')[2]) == 0 and int(ip.split('.')[3]) == 0:
    print('unassigned')
else:
    print('Unused')