#!/bin/env python3

'''
Задание 7.2
Создать скрипт, который будет обрабатывать конфигурационный файл config_sw1.txt:
имя файла передается как аргумент скрипту.
Скрипт должен возвращать на стандартный поток вывода команды из переданного конфигурационного файла, исключая строки, которые начинаются с !.

Между строками не должно быть дополнительного символа перевода строки.

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''

'''
Задание 7.2a
Сделать копию скрипта задания 7.2.

Дополнить скрипт: Скрипт не должен выводить команды, в которых содержатся слова, которые указаны в списке ignore.

Ограничение: Все задания надо выполнять используя только пройденные темы.

ignore = ['duplex', 'alias', 'Current configuration']
'''
from sys import argv
## in case when script start as "python3 <script_name config_sw1.txt>
file = argv[1]
#file ='config_sw1.txt'
## in case interactive question please input connfig_sw1.txt>
#file = input('Введите аргумент config_sw1.txt \n')

with open(file, 'r') as flow:
    for record in flow:
        if record.strip().startswith('!') or record.strip().startswith('duplex') or record.strip().startswith('alias') or record.strip().startswith('Current configuration'):
            i='i' #Заглушка
        else:
            print(record.strip())
