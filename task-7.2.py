#!/bin/env python3

'''
Задание 7.2
Создать скрипт, который будет обрабатывать конфигурационный файл config_sw1.txt:
имя файла передается как аргумент скрипту.
Скрипт должен возвращать на стандартный поток вывода команды из переданного конфигурационного файла, исключая строки, которые начинаются с !.

Между строками не должно быть дополнительного символа перевода строки.

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''
from sys import argv
## in case when script start as "python3 <script_name config_sw1.txt>
file = argv[1]

## in case interactive question please input connfig_sw1.txt>
#file = input('Введите аргумент config_sw1.txt \n')

with open(file, 'r') as flow:
    for record in flow:
        if record.strip().startswith('!'):
            i='i' #Заглушка
        else:
            print(record.strip())
