#!/bin/env python3
"""
Задание 5.2
Запросить у пользователя ввод IP-сети в формате: 10.1.1.0/24

Затем вывести информацию о сети и маске в таком формате:

Network:
10        1         1         0
00001010  00000001  00000001  00000000

Mask:
/24
255       255       255       0
11111111  11111111  11111111  00000000
Проверить работу скрипта на разных комбинациях сеть/маска.
"""

ip = input('Введите IP & mask in format xxx.xxx.xxx.xxx/yy \n')

str = ip.split('.')
mask_int = int(str[3].split('/')[1])
mask = ''.join(list((bin(2**32-2**mask_int))[2::]))

print(f'''
IP address:
{str[0]:<8} {str[1]:<8} {str[2]:<8} {str[3].split('/')[0]:<8}
{int(str[0]):08b} {int(str[1]):08b} {int(str[2]):08b} {int(str[3].split('/')[0]):08b}
''')

print(f'''
Mask:
{(256-2**mask[-8::1].count('1')):<8} {(256-2**mask[-16:-8].count('1')):<8} {(256-2**mask[-24:-16].count('1')):<8} {(256-2**mask[-32:-24].count('1')):<8}
{(256-2**mask[-8::1].count('1')):08b} {(256-2**mask[-16:-8].count('1')):08b} {(256-2**mask[-24:-16].count('1')):08b} {(256-2**mask[-32:-24].count('1')):08b}
''')

